import keras
from keras.models import Sequential
from keras.layers import BatchNormalization, Conv2D, UpSampling2D, MaxPooling2D, Dropout
from keras.optimizers import Adam, SGD
from keras import regularizers
from keras.callbacks import LearningRateScheduler
import numpy as np
import itertools
import pickle
import sys
import glob
import re
import os
from keras.models import load_model
from keras.callbacks import ModelCheckpoint,EarlyStopping,ReduceLROnPlateau

import tensorflow as tf
#from tensorflow import keras
#from tensorflow.keras import layers

import matplotlib.pyplot as plt 
plt.rcParams["figure.figsize"] = (10,10)


def plot_importance(importances, file_out):
    indices = np.argsort(importances)
    plt.title('Feature Importances')
    plt.barh(range(len(indices)), importances[indices], color='b', align='center')
    plt.yticks(range(len(indices)), [features[i] for i in indices])
    plt.xlabel('Relative Importance')
    plt.savefig(file_out)


year_ini = 2017
year_fin = 2017

fmodel = './fs_aira2VGG16.h5'


importance_patt = 'importance_{}.png'
for year in range(year_ini, year_fin+1):
    importance_file = importance_patt.format(year)

    model = load_model(fmodel) 

    cancelout_feature_importance_sigmoid = model.get_weights()[0]
    print(cancelout_feature_importance_sigmoid)

    plot_importance(cancelout_feature_importance_sigmoid, importance_file)
   

sys.exit(0)


