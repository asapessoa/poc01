import numpy as np
import sys
import glob
import re
from sklearn.model_selection import train_test_split
import random


#seed = 99

#Load data
files = glob.glob("./dados/*_Geopotential.npy")


#f = files.pop(0)

#m = re.search("(\d{4})_", f)
#ano = m.group(1)
#print(ano)


#x = np.load(f)
#print(f)
#print(files)

#print(x.shape)
#x = np.moveaxis(x,1,-1)
#print(x.shape)

#idxs = np.arange(x.shape[0])
#np.random.seed(seed)
#np.random.shuffle(idxs)


#print(idxs.shape)


#X_idx_trn, X_idx_tst, y_idx_trn, y_idx_tst = train_test_split(idxs,idxs,test_size=0.2, shuffle=True, random_state = seed )

#print(X_idx_trn)
#print(X_idx_trn.shape)
#print(y_idx_trn.shape)
#print(X_idx_tst.shape)
#print(y_idx_tst.shape)



#sys.exit()


#outfile = "./inputs_1980_2010.npy"
#np.save(outfile, x)


#sys.exit()



for f in files:
    print("[I] File: %s" % (f))


    m = re.search("(\d{4})_", f)
    yearf = m.group(1)

    x = np.load(f)

    seed = random.randint(10,1000)
    print("[I] Seed: %s" % (seed))

    idxs = np.arange(x.shape[0])
    idxs_trn, idxs_tst_val, _, _ = train_test_split(idxs,idxs,test_size=0.2, shuffle=True, random_state =  seed)
    idxs_tst, idxs_val, _, _ = train_test_split(idxs_tst_val,idxs_tst_val,test_size=0.2, shuffle=True, random_state =  seed)

    print("TRN: %s" % idxs_trn.size)
    print("TST: %s" % idxs_tst.size)
    print("VAL: %s" % idxs_val.size)

    outfile = "./idxs/%s_idxs.npz" % (yearf)

    np.savez(outfile, idxs_trn=idxs_trn, idxs_tst=idxs_tst, idxs_val=idxs_val)

    #tmp = np.load(f)
    #tmp = np.moveaxis(tmp,1,-1)
    #x = np.concatenate((x,tmp),axis=0)
    #print(x.shape)




#print(x.shape)


sys.exit()


