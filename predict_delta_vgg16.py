#from keras.models import Sequential
#from keras.layers import BatchNormalization, Conv2D, UpSampling2D, MaxPooling2D, Dropout
#from keras.optimizers import Adam, SGD
#from keras import regularizers
#from keras.callbacks import LearningRateScheduler
#import tensorflow as tf
from keras.models import load_model
import numpy as np
import itertools
import pickle
import sys
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.basemap import Basemap
from mpl_toolkits.basemap import shiftgrid
from matplotlib.patches import Polygon
import pickle

def make_map(lat,lon):
    fig, ax = plt.subplots()
    m = Basemap(projection='cyl', llcrnrlon=lon.min(), 
            urcrnrlon=lon.max(),llcrnrlat=lat.min(),
            urcrnrlat=lat.max(), resolution='h')
    
    m.ax = ax
    m.drawcoastlines()
    m.drawmapboundary()
    m.drawparallels(np.arange(-90.,120.,30.),labels=[1,0,0,0])
    m.drawmeridians(np.arange(-180.,180.,30.),labels=[0,0,0,1])
    
    #m.fillcontinents()
    return fig, m

#def drawstates(ax, shapefile='/home/operacao/alex.pessoa/aira2/GIS/gadm36_BRA_1'):
#        shp = m.readshapefile(shapefile, 'states', drawbounds=True)
#        for nshape, seg in enumerate(m.states):
#            #poly = Polygon(seg, facecolor='0.75', edgecolor='k',fill=False)
#            poly = Polygon(seg, edgecolor='k',fill=False)
#            ax.add_patch(poly)


def plot_map(lon,lat,file_name,data,title,cmap):
    plt.figure(figsize=(8,6))
    fig, m = make_map(lat,lon)
    m.drawcountries()
    _ = m.drawstates()
    #m = drawstates(m)
    plt.title(title)

    x, y = m(lon, lat)

    contourf3 = m.contourf(x, y, np.squeeze(data),cmap=cmap)

    m.colorbar(contourf3, location='bottom', pad="10%")
    plt.savefig(file_name)

    plt.close('all')

    return 0


def make_input(inputs, year):
    print("[I] Processing the inputs files")
    res = np.load(inputs[0].format(year))
    inputs = np.delete(inputs,0)
    for fin in inputs:
        fin = fin.format(year)
        #print(fin)
        tmp = np.load(fin)

        if tmp.shape[0] == 1:
           tmp = np.moveaxis(tmp,0,1)

        res = np.concatenate((tmp,res), axis = 1)

    res = np.moveaxis(res,1,-1)
    return res


#print(tf.__version__)

# Set CPU as available physical device
#my_devices = tf.config.experimental.list_physical_devices(device_type='CPU')
#tf.config.experimental.set_visible_devices(devices= my_devices, device_type='CPU')
#tf.debugging.set_log_device_placement(True)

input_patt = ['./dados/{}_Geopotential.npy',
              './dados/Temperature__{}_t.npy',
              './dados/Convective_available_{}_cape.npy',
              './dados/Divergence__{}_d.npy',
              './dados/Relative_humidity_{}_r.npy'
             ]


lon_file = './dados/longitude_precip.npy'
lat_file = './dados/latitude_precip.npy'

#target_patt = './dados/{}_Convective_precipitation.npy'
target_patt = './dados/hourly_precipitation_{}.npy'
idxs_patt = './idxs/{}_idxs.npz'

year_ini = 2017
year_fin = 2017

fmodel = './delta_aira2VGG16.h5'

delta = 3


#Configure the lon and lat array
yi = 3
yf = -3
xi = 3
xf = -4
lons = np.load(lon_file)
lats = np.load(lat_file)

#print(lats.shape)
#print(lons.shape)

lons = lons[xi:xf]
lats = lats[yi:yf]

lon, lat = np.meshgrid(lons,lats)

#print(lat.shape)
#print(lon.shape)
#print(lat)
#print(lon)
#sys.exit()


#with tf.device('/CPU:0'):
model = load_model(fmodel)
print(model.summary())

for year in range(year_ini, year_fin+1):
    #fin = input_patt.format(year)
    ftarget = target_patt.format(year)
    fidxs = idxs_patt.format(year)

    x = make_input(input_patt, year)

    #print("[I] File in: %s" % (fin))
    print("[I] File target: %s" % (ftarget))
    print("[I] File idxs: %s" % (fidxs))

    #x = np.load(fin)
    #x = np.moveaxis(x,1,-1)

    y = np.load(ftarget)
    y = np.moveaxis(y,1,-1)

    xi = 4
    xf = -4
    yi = 4
    yf = -5

    x = x[:,xi:xf,yi:yf,:]

    xi = 3
    xf = -3
    yi = 3
    yf = -4
    y = y[:,xi:xf,yi:yf,:]

    nzfile = np.load(fidxs)
    idxs_tst = nzfile['idxs_tst']

    x_test = x[idxs_tst,:,:,:]
    y_test = y[idxs_tst,:,:,:]

    x_test = x_test[:(delta*-1),:,:,:]
    y_test = y_test[delta:,:]

    print(x.shape)
    print(y.shape)
    print(idxs_tst.shape)
    print(x_test.shape)
    print(y_test.shape)

    nx = x.shape[1]
    ny = x.shape[2]
    nz = x.shape[3]
    print(nx)
    print(ny)
    print(nz)

    import datetime

    rain = model.predict(x_test)
    print(rain.shape)
    for i in range(0,10):
        doy = idxs_tst[i]/24
        dt = datetime.datetime(year, 1, 1) + datetime.timedelta(doy)

        title = "VGG16 Predition - %s" % (dt)
        file_name = './output/rain_vgg_{}_{}_pred.png'.format(year,i)
        plot_map(lon,lat,file_name,rain[i,:,:,0],title,'Blues')

        title = "Real - %s" % (dt)
        file_name = './output/rain_vgg_{}_{}_real.png'.format(year,i)
        plot_map(lon,lat,file_name,y_test[i,:,:,0],title,'Blues')

        title = "VGG16 Predition Error- %s" % (dt)
        file_name = './output/rain_vgg_{}_{}_erro.png'.format(year,i)
        plot_map(lon,lat,file_name,(y_test[i,:,:,0]-rain[i,:,:,0]),title,'coolwarm')

   
    with open('vgg16_{}.pkl'.format(year), 'wb') as f:
         pickle.dump([rain,y_test], f)

    print("--")
    #sys.exit()


sys.exit(0)

