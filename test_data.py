import numpy as np
import itertools
import pickle
import sys
import glob
import re
import os


def make_input(inputs, year):
    print("[I] Processing the inputs files")
    res = np.load(inputs[0].format(year))
    inputs = np.delete(inputs,0)
    for fin in inputs:
        fin = fin.format(year)
        #print(fin)
        tmp = np.load(fin)

        if tmp.shape[0] == 1:
           tmp = np.moveaxis(tmp,0,1)

        res = np.concatenate((tmp,res), axis = 1)

    res = np.moveaxis(res,1,-1)
    return res



input_patt = ['./dados/{}_Geopotential.npy',
              './dados/Temperature__{}_t.npy',
              './dados/Convective_available_{}_cape.npy',
              './dados/Divergence__{}_d.npy',
              './dados/Relative_humidity_{}_r.npy',
              './dados/wind_speed_10_meters_{}.npy' 
             ]

target_patt = './dados/hourly_precipitation_{}.npy'
idxs_patt = './idxs/{}_idxs.npz'

year_ini = 2010
year_fin = 2010




for year in range(year_ini, year_fin+1):

    #make the input
    x = make_input(input_patt, year)

    ftarget = target_patt.format(year)

    y = np.load(ftarget)
    y = np.moveaxis(y,1,-1)

     
    xi = 4
    xf = -4
    yi = 4
    yf = -5 
    x = x[:,xi:xf,yi:yf,:]


    xi = 3
    xf = -3
    yi = 3
    yf = -4 
    y = y[:,xi:xf,yi:yf,:]



    print(x.shape)
    print(y.shape)

    n0 = x.shape[0]
    nx = x.shape[1]
    ny = x.shape[2]
    nz = x.shape[3]
    print(n0)
    print(nx)
    print(ny)
    print(nz)

    print("--")

    #sys.exit()



#print(x.shape)

sys.exit(0)


