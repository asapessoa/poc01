import keras
from keras.models import Sequential
from keras.layers import BatchNormalization, Conv2D, UpSampling2D, MaxPooling2D, Dropout
from keras.optimizers import Adam, SGD
from keras import regularizers
from keras.callbacks import LearningRateScheduler
import numpy as np
import itertools
import pickle
import sys
import glob
import re
import os
from keras.models import load_model
from keras.callbacks import ModelCheckpoint,EarlyStopping,ReduceLROnPlateau

import tensorflow as tf
#from tensorflow import keras
#from tensorflow.keras import layers

import matplotlib.pyplot as plt 
plt.rcParams["figure.figsize"] = (10,10)


class CancelOut(keras.layers.Layer):
    ''' 
    CancelOut layer, keras implementation. 
    BORISOV, Vadim; HAUG, Johannes; KASNECI, Gjergji. CancelOut: A Layer for Feature Selection in Deep Neural Networks.
            In: International Conference on Artificial Neural Networks. Springer, Cham, 2019. p. 72-83.
    '''
    def __init__(self, activation='sigmoid', cancelout_loss=True, lambda_1=0.002, lambda_2=0.001):
        super(CancelOut, self).__init__()
        self.lambda_1 = lambda_1
        self.lambda_2 = lambda_2
        self.cancelout_loss = cancelout_loss
    
        if activation == 'relu': self.activation = tf.nn.relu
        if activation == 'sigmoid': self.activation = tf.sigmoid
        if activation == 'softmax': self.activation = tf.nn.softmax

    def build(self, input_shape):
        self.w = self.add_weight(
            shape=(input_shape[-1],),
            initializer='ones',
            trainable=True
        )   
    def call(self, inputs):
        if self.cancelout_loss:
            self.add_loss( self.lambda_1 * tf.norm(self.w, ord=1) + self.lambda_2 * tf.norm(self.w, ord=2))
        return tf.math.multiply(inputs, self.activation(self.w))
    
    
    def get_config(self):
        return {"activation": self.activation} 



def plot_importance(importances, file_out):
    indices = np.argsort(importances)
    plt.title('Feature Importances')
    plt.barh(range(len(indices)), importances[indices], color='b', align='center')
    plt.yticks(range(len(indices)), [features[i] for i in indices])
    plt.xlabel('Relative Importance')
    plt.savefig(file_out)



 
def get_vgg16(nx,ny,nz):
    model = Sequential()

    # Encoder
    # Block 1
    model.add(BatchNormalization(axis=3, input_shape=(nx, ny, nz)))
    model.add(CancelOut(activation='sigmoid'))

    #model.add(Dropout(.2))
    #model.add(Conv2D(64, (3, 3), padding='same', activation='relu', bias_regularizer=regularizers.l1(0.01), name='block1_conv1', input_shape=(nx,ny,nz)))
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu', bias_regularizer=regularizers.l1(0.01), name='block1_conv1'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu', bias_regularizer=regularizers.l1(0.01), name='block1_conv2'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2), name='block1_pool'))

    # Block 2
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(128, (3, 3), padding='same', activation='relu', bias_regularizer=regularizers.l1(0.01), name='block2_conv1'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(128, (3, 3), padding='same', activation='relu', bias_regularizer=regularizers.l1(0.01), name='block2_conv2'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2), name='block2_pool'))

    # Block 3
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(256, (3, 3), padding='same', activation='relu', bias_regularizer=regularizers.l1(0.01), name='block3_conv1'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(256, (3, 3), padding='same', activation='relu', bias_regularizer=regularizers.l1(0.01), name='block3_conv2'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2), name='block3_pool'))

    # Block 4
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(512, (3, 3), padding='same', activation='relu', bias_regularizer=regularizers.l1(0.01), name='block4_conv1'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(512, (3, 3), padding='same', activation='relu', bias_regularizer=regularizers.l1(0.01), name='block4_conv2'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2), name='block4_pool'))


    # Block 5
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(512, (3, 3), padding='same', activation='relu', bias_regularizer=regularizers.l1(0.01), name='block5_conv1'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(512, (3, 3), padding='same', activation='relu', bias_regularizer=regularizers.l1(0.01), name='block5_conv2'))

    # Decoder
    # Block 6
    model.add(UpSampling2D((2, 2), name='block6_upsampl'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(512, (3, 3), padding='same', activation='relu', bias_regularizer=regularizers.l1(0.01), name='block6_conv1'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(512, (3, 3), padding='same', activation='relu', bias_regularizer=regularizers.l1(0.01), name='block6_conv2'))

    # Block 7
    model.add(UpSampling2D((2, 2), name='block7_upsampl'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(256, (3, 3), padding='same', activation='relu', bias_regularizer=regularizers.l1(0.01), name='block7_conv1'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(256, (3, 3), padding='same', activation='relu', bias_regularizer=regularizers.l1(0.01), name='block7_conv2'))

    # Block 8
    model.add(UpSampling2D((2, 2), name='block8_upsampl'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(128, (3, 3), padding='same', activation='relu', bias_regularizer=regularizers.l1(0.01), name='block8_conv1'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(128, (3, 3), padding='same', activation='relu', bias_regularizer=regularizers.l1(0.01), name='block8_conv2'))

    # Block 9
    model.add(UpSampling2D((2, 2), name='block9_upsampl'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu', bias_regularizer=regularizers.l1(0.01), name='block9_conv1'))
    model.add(BatchNormalization(axis=3))
    #model.add(Dropout(.2))
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu', bias_regularizer=regularizers.l1(0.01), name='block9_conv2'))

    # Output
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(1, (1, 1), padding='same', activation='relu', bias_regularizer=regularizers.l1(0.01), name='block10_conv1'))

    sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
    model.compile(loss='mae', optimizer=sgd, metrics=['mse'])
    #model.compile(loss='mae', optimizer=Adam(lr=0.001), metrics=['mse'])
    print(model.summary())

    return model

def make_input(inputs, year):
    print("[I] Processing the inputs files")
    res = np.load(inputs[0].format(year))
    inputs = np.delete(inputs,0)
    for fin in inputs:
        fin = fin.format(year)
        #print(fin)
        tmp = np.load(fin)

        if tmp.shape[0] == 1:
           tmp = np.moveaxis(tmp,0,1)

        res = np.concatenate((tmp,res), axis = 1)

    res = np.moveaxis(res,1,-1)
    return res


input_patt = ['./dados/{}_Geopotential.npy',
              './dados/Temperature__{}_t.npy',
              './dados/Convective_available_{}_cape.npy',
              './dados/Divergence__{}_d.npy',
              './dados/Relative_humidity_{}_r.npy',
              './dados/dew_temperature_{}.npy',
              './dados/pressure_{}.npy',
              './dados/wind_speed_10_meters_{}.npy'
             ]


features = ['Geopotential_l1','Geopotential_l2','Geopotential_l3',
            'Temperature_l1','Temperature_l2','Temperature_l3',
            'CAPE',
            'Divergence_l1','Divergence_l2','Divergence_l3',
            'Rel_humidity',
            'dew_temperature',
            'pressure',
            'wind_speed'
             ]



target_patt = './dados/hourly_precipitation_{}.npy'
idxs_patt = './idxs/{}_idxs.npz'

year_ini = 2017
year_fin = 2017

fmodel = './fs_aira2VGG16.h5'



resume_trn = 0
if resume_trn:
   print("[I] Resume mode is ON!")
else:
   print("[I] Resume mode is OFF.")

epochs = 1000
batch_size = 32
#best_model = "./models/model-{epoch:02d}-{val_mse:.4f}.h5"
best_model_pattern = "./models/best_{}.h5"
monitor_metric = 'val_mse'
verb = True
earlystop_mode = 'auto'
earlystop_monitor = 'val_mse'
earlystop_patience = 10
importance_patt = 'importance_{}.png'

first = True
for year in range(year_ini, year_fin+1):
    importance_file = importance_patt.format(year)
    best_model = best_model_pattern.format(year)

    #make the input
    x = make_input(input_patt, year)

    ftarget = target_patt.format(year)
    fidxs = idxs_patt.format(year)

    print("[I] File target: %s" % (ftarget))
    print("[I] File idxs: %s" % (fidxs))

    #x = np.load(fin)
    #x = np.moveaxis(x,1,-1)
 

    y = np.load(ftarget)
    y = np.moveaxis(y,1,-1)

     
    xi = 4
    xf = -4
    yi = 4
    yf = -5 
    x = x[:,xi:xf,yi:yf,:]


    xi = 3
    xf = -3
    yi = 3
    yf = -4 
    y = y[:,xi:xf,yi:yf,:]

    nzfile = np.load(fidxs)
    idxs_trn = nzfile['idxs_trn']
    idxs_tst = nzfile['idxs_tst']


    x_train = x[idxs_trn,:,:,:]
    x_test = x[idxs_tst,:,:,:]

    y_train = y[idxs_trn,:,:,:]
    y_test = y[idxs_tst,:,:,:]

    print(x.shape)
    print(y.shape)
    print(idxs_trn.shape)
    print(idxs_tst.shape)
    print(x_train.shape)
    print(x_test.shape)
    print(y_train.shape)
    print(y_test.shape)

    nx = x.shape[1]
    ny = x.shape[2]
    nz = x.shape[3]
    print(nx)
    print(ny)
    print(nz)
    

    if first:
       first = False
       if resume_trn:
          model = load_model(fmodel) 
       else:
          model = get_vgg16(nx,ny,nz)



    checkpoint = ModelCheckpoint(best_model, monitor=monitor_metric, verbose=verb, save_best_only=True)
    earlystop = EarlyStopping( monitor=earlystop_monitor, patience=earlystop_patience, mode=earlystop_mode, verbose=verb)
    #reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.09, patience=20, min_lr=0.000001)
    callbacks_list = [earlystop]


    history = model.fit(x_train, y_train, epochs=epochs, verbose=verb, validation_data=(x_test, y_test), batch_size=batch_size, callbacks=callbacks_list, shuffle=True)

    print("[I] Saving model")
    model.save(fmodel)
    print("--")

    cancelout_feature_importance_sigmoid = model.get_weights()[0]
    print(cancelout_feature_importance_sigmoid)

    plot_importance(cancelout_feature_importance_sigmoid, importance_file)
   

    #sys.exit()



#print(x.shape)

sys.exit(0)


